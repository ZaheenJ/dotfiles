vim.g.mapleader = " " -- make sure to set `mapleader` before lazy so your mappings are correct
vim.o.termguicolors = true

-- Bootstrapping plugin manager (lazy.nvim)

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "--branch=stable", -- remove this if you want to bootstrap to HEAD
    "https://github.com/folke/lazy.nvim.git",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
  {
    "catppuccin/nvim",
    name = "catppuccin",

    lazy = false, -- Main colorscheme must not be lazy-loaded according
    priority = 1000, -- Main colorscheme must be loaded before everyting else

    config = {
      flavour = "mocha", -- latte, frappe, macchiato, mocha
      term_colors = true,
      transparent_background = true,
      no_italic = false,
      no_bold = false,
      styles = {
        comments = { "italic" },
        conditionals = {},
        loops = {},
        functions = {},
        keywords = {},
        strings = {},
        variables = {},
        numbers = {},
        booleans = {},
        properties = {},
        types = {},
      },
      color_overrides = {
        mocha = {
          -- base = "#000000",
        },
      },
      highlight_overrides = {
        mocha = function(C)
          return {
            -- -- TabLineSel = { bg = C.pink },
            -- NvimTreeNormal = { bg = C.none },
            -- CmpBorder = { fg = C.surface2 },
            -- -- Pmenu = { bg = C.black },
            -- -- -- Pmenu = { bg = C.black },
            -- -- TelescopeBorder = { link = "FloatBorder" },
            -- Normal = { bg = C.none },
            -- NormalNC = { bg = C.none },
            -- NormalSB = { bg = C.none },
            -- CursorLine = { bg = C.none },
            -- VertSplit = { bg = C.none },
            -- Folded = { bg = C.none },
          }
        end,
      },
      integrations = {
        treesitter = true,
        ts_rainbow = true,
        nvimtree = true,
        which_key = true,
        gitsigns = true,
        cmp = true,
        telescope = true,
        lsp_trouble = true,
        native_lsp = {
          enabled = true,
          virtual_text = {
            errors = { "italic" },
            hints = { "italic" },
            warnings = { "italic" },
            information = { "italic" },
          },
          underlines = {
            errors = { "underline" },
            hints = { "underline" },
            warnings = { "underline" },
            information = { "underline" },
          },
        },
        integration = {
          dap = {
            enabled = true,
            enable_ui = true, -- enable nvim-dap-ui
          }
        },
        -- navic = { enabled = true, custom_bg = "NONE", },
      },
    },
  },

  {
    "nvim-treesitter/nvim-treesitter",
    build = function()
      require('nvim-treesitter.install').update({ with_sync = true })
    end,
    dependencies = {
    },

    config = function()
      require('nvim-treesitter.configs').setup {
        -- Markdown for lspsaga
        ensure_installed = { "c", "cpp", "rust", "python", "lua", "bash", "fish", "make", "cmake", "meson", "ninja", "markdown", "markdown_inline",
          "toml", "json", "json5", "gitcommit", "diff", "css", "scss" },
        auto_install = true,

        highlight = { enable = true, },
        indent = { enable = true, },
        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = '<c-space>',
            node_incremental = '<c-space>',
            scope_incremental = '<c-s>',
            node_decremental = '<c-backspace>',
          },
        },
        rainbow = {
          enable = true,
          extended_mode = true,
          max_file_lines = nil,
        },
        -- textobjects = { },
      }
    end,
  },

  {
    "p00f/nvim-ts-rainbow",
    dependencies = { "nvim-treesitter/nvim-treesitter", },
  },

  {
    "nvim-treesitter/nvim-treesitter-textobjects",
    dependencies = { "nvim-treesitter/nvim-treesitter", },
  },

  --[[ { -- TODO: Replace with nvim-navic, or just use lspsaga winbar
    "nvim-treesitter/nvim-treesitter-context",
    dependencies = { "nvim-treesitter/nvim-treesitter", },

    config = {
      enable = true,

      max_lines = 3,
      patterns = {
        rust = { 'impl_item', },
        json = { 'pair' }
      },
    },
  }, ]]

  -- {
  --   "SmiteshP/nvim-navic",
  --
  --   config = {
  --     highlight = true,
  --   }
  -- },

  {
    "norcalli/nvim-colorizer.lua",

    config = true,
  },

  {
    "lukas-reineke/indent-blankline.nvim",

    config = function()
      vim.opt.list = true
      vim.opt.listchars:append "eol:↴"

      require("indent_blankline").setup {}
    end,
  },

  {
    "Darazaki/indent-o-matic",

    config = {
      max_lines = 2048,
      standard_widths = { 2, 4, 8 },

      filetype_rust = { standard_widths = { 4 }, },
      filetype_lua = { standard_widths = { 2 } },
    },
  },

  {
    "numToStr/Comment.nvim",

    config = true,
  },

  {
    "kylechui/nvim-surround",

    config = true,
  },

  {
    "windwp/nvim-autopairs",

    config = {
      check_ts = true,
    },
  },

  {
    "lewis6991/gitsigns.nvim",
    config = true,
    -- TODO: Understanding
  },

  { -- TODO: Possibly replace with neo-tree.nvim + keybindings
    "nvim-tree/nvim-tree.lua",
    dependencies = { "nvim-tree/nvim-web-devicons", },

    config = {
      disable_netrw = true,
      sort_by = "extension",
      diagnostics = { enable = true },
      modified = { enable = true, },
      renderer = { group_empty = true, },
    },
  },

  {
    "folke/which-key.nvim",

    config = true,
  },

  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "SmiteshP/nvim-navic",
    }, -- Plugins that need to change capabilities or modify on_attach

    config = function()
      local lspconfig = require('lspconfig')
      -- Add additional capabilities supported by nvim-cmp
      local capabilities = require("cmp_nvim_lsp").default_capabilities()

      -- Enable some language servers with the additional completion capabilities offered by nvim-cmp
      local no_conf_servers = { 'clangd', 'pyright' } -- not including rust_analyzer which is set up by rust-tools.nvim
      for _, lsp in ipairs(no_conf_servers) do
        lspconfig[lsp].setup {
          on_attach = function(client, bufnr)
            if client.server_capabilities.documentSymbolProvider then
              require("nvim-navic").attach(client, bufnr)
            end
          end,
          capabilities = capabilities,
        }
      end

      lspconfig.lua_ls.setup {
        capabilities = capabilities,

        settings = {
          Lua = {
            runtime = {
              -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
              version = 'LuaJIT',
            },
            diagnostics = {
              -- Get the language server to recognize the `vim` global
              globals = { 'vim' },
            },
            workspace = {
              -- Make the server aware of Neovim runtime files
              library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
              enable = false,
            },
          },
        },
      }

      local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
      for type, icon in pairs(signs) do
        local hl = "DiagnosticSign" .. type
        vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
      end
    end
  },

  {
    "glepnir/lspsaga.nvim",
    event = "BufRead",

    config = function()
      require('lspsaga').setup({
        ui = {
          border = "rounded",
          colors = {
            normal_bg = "",
          }
        },
        symbol_in_winbar = {
          separator = " > "
        }
      })

      local keymap = vim.keymap.set
      local opts = { silent = true }

      -- Lsp finder find the symbol definition implement reference
      -- if there is no implement it will hide
      -- when you use action in finder like open vsplit then you can
      -- use <C-t> to jump back
      keymap("n", "gh", "<cmd>Lspsaga lsp_finder<CR>", opts)

      -- Code action
      keymap({ "n", "v" }, "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts)

      -- Rename
      keymap("n", "gr", "<cmd>Lspsaga rename<CR>", opts)

      -- Peek Definition
      -- you can edit the definition file in this flaotwindow
      -- also support open/vsplit/etc operation check definition_action_keys
      -- support tagstack C-t jump back
      keymap("n", "gd", "<cmd>Lspsaga peek_definition<CR>", opts)

      -- Show line diagnostics
      keymap("n", "<leader>cd", "<cmd>Lspsaga show_line_diagnostics<CR>", opts)

      -- Show cursor diagnostics
      keymap("n", "<leader>cd", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts)

      -- Diagnostic jump can use `<c-o>` to jump back
      keymap("n", "[e", "<cmd>Lspsaga diagnostic_jump_prev<CR>", opts)
      keymap("n", "]e", "<cmd>Lspsaga diagnostic_jump_next<CR>", opts)

      -- Only jump to error
      keymap("n", "[E", function()
        require("lspsaga.diagnostic").goto_prev({ severity = vim.diagnostic.severity.ERROR })
      end, opts)
      keymap("n", "]E", function()
        require("lspsaga.diagnostic").goto_next({ severity = vim.diagnostic.severity.ERROR })
      end, opts)

      -- Outline
      keymap("n", "<leader>o", "<cmd>Lspsaga outline<CR>", opts)

      -- Hover Doc
      keymap("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts)

      -- Formatting
      keymap("n", "<leader>f", function() vim.lsp.buf.format { async = true } end, opts)

      -- Float terminal
      keymap("n", "<A-d>", "<cmd>Lspsaga open_floaterm<CR>", opts)
      -- if you want to pass some cli command into a terminal you can do it like this
      -- open lazygit in lspsaga float terminal
      -- keymap("n", "<A-d>", "<cmd>Lspsaga open_floaterm lazygit<CR>", opts)
      -- close floaterm
      keymap("t", "<A-d>", [[<C-\><C-n><cmd>Lspsaga close_floaterm<CR>]], opts)
    end
  },

  {
    "L3MON4D3/LuaSnip",
    dependencies = { "rafamadriz/friendly-snippets", },

    config = function()
      require("luasnip.loaders.from_vscode").lazy_load()
    end
  },

  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "saadparwaiz1/cmp_luasnip",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      -- "Saecki/crates.nvim",
      "L3MON4D3/LuaSnip",
      "windwp/nvim-autopairs",
      "onsails/lspkind.nvim"
    },

    config = function()
      -- luasnip setup
      local luasnip = require 'luasnip'

      -- nvim-cmp setup
      local cmp = require 'cmp'
      cmp.setup {
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },
        mapping = cmp.mapping.preset.insert({
          ['<C-d>'] = cmp.mapping.scroll_docs(-4),
          ['<C-f>'] = cmp.mapping.scroll_docs(4),
          ['<C-Space>'] = cmp.mapping.complete(),
          ['<CR>'] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
          },
          ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
              luasnip.expand_or_jump()
            else
              fallback()
            end
          end, { 'i', 's' }),
          ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
              luasnip.jump(-1)
            else
              fallback()
            end
          end, { 'i', 's' }),
        }),
        sources = {
          { name = 'path' },
          { name = 'buffer' },
          { name = 'nvim_lsp' },
          { name = 'luasnip' },
          { name = 'crates' },
        },
        formatting = {
          format = require('lspkind').cmp_format({
            mode = "symbol_text",
            menu = ({
              buffer = "[Buffer]",
              nvim_lsp = "[LSP]",
              luasnip = "[LuaSnip]",
              nvim_lua = "[Lua]",
              latex_symbols = "[Latex]",
            })
          }),
        },
        view = {
          entries = { name = 'custom', selection_order = 'near_cursor' }
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
      }
      cmp.event:on(
        'confirm_done',
        require('nvim-autopairs.completion.cmp').on_confirm_done()
      )
      -- `/` cmdline setup.
      cmp.setup.cmdline('/', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = {
          { name = 'buffer' }
        }
      })
      cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
          { name = 'path' }
        }, {
          {
            name = 'cmdline',
            option = {
              ignore_cmds = { 'Man', '!' }
            }
          }
        })
      })
    end
  },

  {
    "ray-x/lsp_signature.nvim",

    config = true,
  },

  {
    "folke/trouble.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons", },

    config = function()
      require("trouble").setup {}

      local opts = { silent = true, noremap = true }
      vim.keymap.set("n", "<leader>xx", "<cmd>TroubleToggle<cr>", opts)
      vim.keymap.set("n", "<leader>xw", "<cmd>TroubleToggle workspace_diagnostics<cr>", opts)
      vim.keymap.set("n", "<leader>xd", "<cmd>TroubleToggle document_diagnostics<cr>", opts)
      vim.keymap.set("n", "<leader>xl", "<cmd>TroubleToggle loclist<cr>", opts)
      vim.keymap.set("n", "<leader>xq", "<cmd>TroubleToggle quickfix<cr>", opts)
      vim.keymap.set("n", "gR", "<cmd>TroubleToggle lsp_references<cr>", opts)
    end
  },

  {
    "folke/todo-comments.nvim",
    dependencies = { "nvim-lua/plenary.nvim", },

    config = function()
      require("todo-comments").setup {}

      local keymap = vim.keymap.set

      keymap("n", "]t", function()
        require("todo-comments").jump_next()
      end, { desc = "Next todo comment" })

      keymap("n", "[t", function()
        require("todo-comments").jump_prev()
      end, { desc = "Previous todo comment" })

      keymap("n", "<leader>xt", "<cmd>TodoTrouble<cr>")
      keymap("n", "<leader>tt", "<cmd>TodoTelescope<cr>")
    end,
  },

  {
    "nvim-telescope/telescope.nvim",
    dependencies = { "nvim-lua/plenary.nvim", },

    config = function()
      local keymap = vim.keymap.set
      local builtin = require("telescope.builtin")

      keymap('n', '<leader>tf', builtin.find_files, {})
      keymap('n', '<leader>ts', builtin.live_grep, {})
      keymap('n', '<leader>tb', builtin.buffers, {})
      keymap('n', '<leader>tk', builtin.keymaps, {})
      keymap('n', '<leader>th', builtin.help_tags, {})
      keymap('n', '<leader>to', builtin.vim_options, {})
      keymap('n', '<leader>tgc', builtin.git_commits, {})
      keymap('n', '<leader>tgb', builtin.git_branches, {})
      keymap('n', '<leader>tgs', builtin.git_status, {})

      require("telescope").setup {
        defaults = {
          mappings = {
            i = {
              ["<C-h>"] = "which_key"
            },
            n = {
              ["<C-h>"] = "which_key"
            },
          },
        },
      }
    end,
  },

  {
    "sudormrfbin/cheatsheet.nvim",
    dependencies = { "nvim-telescope/telescope.nvim" },
  },

  { -- TODO key-bindings, other setup
    "saecki/crates.nvim",

    event = "BufRead Cargo.toml",
    config = true,
  },

  -- TODO: linting with nvim-lint or null-ls

  {
    "rcarriga/nvim-dap-ui",
    dependencies = { "mfussenegger/nvim-dap" },

    config = function()
      local dap, dapui = require("dap"), require("dapui")
      dapui.setup({
        layouts = {
          {
            elements = {
              "breakpoints",
              "watches",
              "scopes",
              "stacks",
            },

            position = "left",
            size = 0.25,
          },

          {
            elements = {
              "breakpoints",
              "watches",
              "scopes",
            },

            position = "left",
            size = 0.25,
          },

          {
            elements = {
              "console",
            },

            position = "bottom",
            size = 0.25,
          },

          {
            elements = {
              "repl",
            },

            position = "bottom",
            size=0.25
          },
        },

        controls = { enabled = false, },

        floating = { border = "rounded", },
      })

      dap.adapters.codelldb = {
        type = 'server',
        port = "${port}",
        executable = {
          command = 'codelldb',
          args = { "--port", "${port}" },
        }
      }

      local confs = {
        {
          name = "Terminal io",
          type = "codelldb",
          request = "launch",
          program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
          end,
          stdio = nil,
          runInTerminal = true,
          terminal = "integrated",
          stopOnEntry = false,
          cwd = '${workspaceFolder}',
          args = {},
        },
        {
          name = "File io",
          type = "codelldb",
          request = "launch",
          program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
          end,
          stdio = { "input.txt", "output.txt", "error.txt" }, -- input, output, error
          stopOnEntry = false,
          cwd = '${workspaceFolder}',
          runInTerminal = false,
          args = {},
        },
        {
          name = "CP, Terminal io",
          type = "codelldb",
          request = "launch",
          program = function()
            return vim.fn.getcwd() .. '/a.out'
          end,
          stdio = nil,
          runInTerminal = true,
          terminal = "integrated",
          stopOnEntry = false,
          cwd = '${workspaceFolder}',
          args = {},
        },
        {
          name = "CP, File io",
          type = "codelldb",
          request = "launch",
          program = function()
            return vim.fn.getcwd() .. '/a.out'
          end,
          stdio = { "input.txt", "output.txt", "error.txt" }, -- input, output, error
          stopOnEntry = false,
          cwd = '${workspaceFolder}',
          runInTerminal = false,
          args = {},
        },
      }
      dap.configurations.cpp = confs
      dap.configurations.c = confs
      dap.configurations.rust = confs

      dap.listeners.after.event_initialized["dapui_config"] = function()
        dapui.toggle({ layout = 2 })
      end
      dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
      end
      dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close()
      end

      local keymap, opts = vim.keymap.set, { silent = true, noremap = true }

      keymap('n', '<F1>', function() dap.step_into() end, opts)
      keymap('n', '<F2>', function() dap.step_over() end, opts)
      keymap('n', '<F3>', function() dap.step_out() end, opts)
      keymap('n', '<F4>', function() dap.continue() end, opts)
      keymap('n', '<leader>b', function() dap.toggle_breakpoint() end, opts)
      keymap('n', '<leader>B', function() dap.set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, opts)
      keymap('n', '<leader>dr', function() dap.run_last() end, opts)
      keymap('n', '<leader>dtt', function() dapui.toggle({ layout = 3 }) end, opts)
      keymap('n', '<leader>dtl', function() dapui.toggle({ layout = 2 }) end, opts)
      keymap('n', '<leader>dts', function() dapui.toggle({ layout = 1 }) end, opts)
      keymap('n', '<leader>dtr', function() dapui.toggle({ layout = 4 }) end, opts)
      keymap('n', '<leader>de', function() dapui.eval() end, opts)

      -- Make catpuccin work
      local sign = vim.fn.sign_define

      sign("DapBreakpoint", { text = "●", texthl = "DapBreakpoint", linehl = "", numhl = "" })
      sign("DapBreakpointCondition", { text = "●", texthl = "DapBreakpointCondition", linehl = "", numhl = "" })
      sign("DapLogPoint", { text = "◆", texthl = "DapLogPoint", linehl = "", numhl = "" })
    end
  },

  {
    "folke/neodev.nvim",
    config = function()
      require("neodev").setup({
        library = { plugins = { "nvim-dap-ui" }, types = true },
      })
    end
  },

  {
    "simrat39/rust-tools.nvim",
    dependencies = {
      "neovim/nvim-lspconfig",
      "nvim-lua/plenary.nvim",
      "mfussenegger/nvim-dap",
    },

    config = function()
      require('rust-tools').setup({
        tools = {
          inlay_hints = {
            -- Pad inlay hints 1 to the right
            parameter_hints_prefix = " <- ",
            other_hints_prefix = " => ",
          },
        },
        server = {
          on_attach = function(client, bufnr)
            if client.server_capabilities.documentSymbolProvider then
              require("nvim-navic").attach(client, bufnr)
            end
          end,

          standalone = false,
        },
        dap = {
          adapter = require('rust-tools.dap').get_codelldb_adapter('codelldb', '/usr/lib/codelldb/lldb/lib/liblldb.so')
        },
        hover_actions = { auto_focus = true, },
      })
    end
  },

  {
    "nvim-lualine/lualine.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons", },

    config = function()
      require('lualine').setup {
        options = {
          theme = "catppuccin"
          -- ... the rest of your lualine config
        },
        inactive_sections = {
          lualine_a = { 'filename' },
          lualine_b = {},
          lualine_c = {},
          lualine_x = {},
          lualine_y = {},
          lualine_z = { 'location' }
        },
        extensions = {
          "nvim-tree",
          "nvim-dap-ui",
          "man"
        }
      }
    end
  },

  { -- TODO: keybindings
    "akinsho/bufferline.nvim",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
      "catppuccin",
    },

    config = function()
      require("bufferline").setup {
        highlights = require("catppuccin.groups.integrations.bufferline").get(),
        options = {
          separator_style = "thick",
          diagnostics = "nvim_lsp",
          diagnostics_indicator = function(count, level, diagnostics_dict, context)
            local icon = level:match("error") and " " or " "
            return " " .. icon .. count
          end,
          offsets = {
            {
              filetype = "NvimTree",
              text = "File Explorer",
              highlight = "Directory",
              separator = true -- use a "true" to enable the default, or set your own character
            }
          },
          hover = {
            enabled = true,
            delay = 200,
            reveal = { 'close' },
          },
        }
      }

      local keymap = vim.keymap.set
      local opts = { noremap = true, silent = true }
      local bufferline = require("bufferline")

      keymap('n', "<leader>1", function() bufferline.go_to_buffer(1) end, opts)
      keymap('n', "<leader>2", function() bufferline.go_to_buffer(2) end, opts)
      keymap('n', "<leader>3", function() bufferline.go_to_buffer(3) end, opts)
      keymap('n', "<leader>4", function() bufferline.go_to_buffer(4) end, opts)
      keymap('n', "<leader>5", function() bufferline.go_to_buffer(5) end, opts)
      keymap('n', "<leader>6", function() bufferline.go_to_buffer(6) end, opts)
      keymap('n', "<leader>7", function() bufferline.go_to_buffer(7) end, opts)
      keymap('n', "<leader>8", function() bufferline.go_to_buffer(8) end, opts)
      keymap('n', "<leader>9", function() bufferline.go_to_buffer(9) end, opts)
      keymap('n', "<leader>$", function() bufferline.go_to_buffer(-1) end, opts)
      keymap('n', "<leader>,", "<cmd>BufferLineCycleNext<cr>", opts)
      keymap('n', "<leader>.", "<cmd>BufferLineCyclePrev<cr>", opts)
      keymap('n', "<leader>>", "<cmd>BufferLineMoveNext<cr>", opts)
      keymap('n', "<leader><", "<cmd>BufferLineMovePrev<cr>", opts)
      keymap('n', "<leader>se", "<cmd>BufferLineSortByExtension<cr>", opts)
      keymap('n', "<leader>sd", "<cmd>BufferLineSortByDirectory<cr>", opts)
      keymap('n', "<leader>p", "<cmd>BufferLinePick<cr>", opts)
      keymap('n', "<leader>P", "<cmd>BufferLinePickClose<cr>", opts)
    end,
  },
}

local opts = {
  ui = {
    border = "rounded",
  },
  install = {
    colorscheme = "catppuccin",
  },
  defaults = {
    version = "*",
  },
  spec = plugins,
}

require("lazy").setup(plugins)

-- vim.cmd.highlight({ "TreesitterContext", "guibg=Black" })

vim.cmd.colorscheme "catppuccin"

vim.o.foldmethod = "expr"
vim.o.foldexpr = "nvim_treesitter#foldexpr()"
vim.o.foldenable = false

vim.o.completeopt = "menu,menuone,noselect"
-- vim.o.clipboard = "unnamedplus"
-- lol

-- vim.o.expandtab = true
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.softtabstop = 4

vim.o.smartindent = true

vim.o.ignore_case = true
vim.o.smartcase = true

vim.o.wrap = false

vim.o.number = true
vim.o.cursorline = true
vim.o.relativenumber = true
vim.o.signcolumn = "yes"

-- vim.o.laststatus = 3
--
vim.o.tildeop = true

-- vim.o.autowrite = true

vim.o.mousemoveevent = true,

vim.api.nvim_create_user_command("DiffOrig", "vert new | read ++edit # | 0d_ | diffthis | wincmd p | diffthis", {})
