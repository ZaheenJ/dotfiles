set PATH $PATH ~/.local/bin

if status is-interactive
    # Commands to run in interactive sessions can go here
    starship init fish | source
    zoxide init fish | source

    fish_vi_key_bindings
    set -g fish_greeting    

    set -gx EDITOR nvim
    # set -gx PAGER nvim
    set -gx MANPAGER 'nvim +Man!'
    set -gx MANWIDTH 999

    alias e=$EDITOR
    alias se="sudo -E $EDITOR"
    alias ls="exa --icons"
    alias ll="ls -l"
    alias la="ls -a"
    alias lla="ls -al"
    alias c="bat"
    alias cpt="cp ~/school/11/cs/usaco/template/cpp/* ."
    alias cptr="cp ~/school/11/cs/usaco/template/rust/* ."
    function usaco
        g++ -std=c++17 -O2 -lm $argv -o (string sub --end=-4 $argv)
    end
    function usaco_dbg
        g++ -std=c++17 -g -lm $argv -o (string sub --end=-4 $argv)
    end
    function fgpl
        for dir in */
              $dir
              git pull
              ..
        end
    end

    abbr g 'git'
  
    abbr ga 'git add'
    abbr gaa 'git add --all'
  
    abbr gb 'git branch'
    abbr gbd 'git branch -D'
  
    abbr gbl 'git blame'
  
    abbr gc 'git commit -v'
    abbr gc! 'git commit -v --amend'
    abbr gcn! 'git commit -v --amend --no-edit'
    abbr gca 'git commit -a -v'
    abbr gca! 'git commit -a -v --amend'
    abbr gcan! 'git commit -a -v --no-edit --amend'
    abbr gcans! 'git commit -a -v -s --no-edit --amend'

    abbr gcl 'git clone --recursive'

    abbr gcf 'git config --list'

    abbr gclean 'git clean -fd'

    abbr gco 'git checkout'
    abbr gcob 'git checkout -b'
    abbr gcom 'git checkout master'
    abbr gcod 'git checkout develop'
    abbr gcof 'git checkout feat/'

    abbr gcp 'git cherry-pick'
    abbr gcpa 'git cherry-pick --abort'
    abbr gcpc 'git cherry-pick --continue'

    abbr gd 'git diff'

    abbr gf 'git fetch'
    abbr gfa 'git fetch --all --prune'
    abbr gfo 'git fetch origin'


    abbr gl 'git log'
    abbr glg 'git log --graph'

    abbr gm 'git merge'
  
    abbr gp 'git push'
    abbr gpf 'git push --force'
    abbr gpt 'git push --tags'
    abbr gptf 'git push --tags --force'
    abbr gpoat 'git push origin --all && git push origin --tags'
    abbr gpoatf 'git push origin --all -f && git push origin --tags -f'

    abbr gpristine 'git reset --hard && git clean -dfx'

    abbr gpl 'git pull'
    abbr gpo 'git pull origin'
    abbr gpom 'git pull origin master'
    abbr gpu 'git pull upstream'
    abbr gpum 'git pull upstream master'

    abbr gr 'git remote -v'
    abbr gra 'git remote add'
    abbr grau 'git remote add upstream'
    abbr grrm 'git remote remove'
    abbr grmv 'git remote rename'
    abbr grset 'git remote set-url'

    abbr grb 'git rebase'
    abbr grba 'git rebase --abort'
    abbr grbc 'git rebase --continue'

    abbr grt 'git reset HEAD'
    abbr grhh 'git reset HEAD --hard'
    abbr grth 'git reset --hard'

    abbr gst 'git status'
    abbr gss 'git status -s'

    abbr gss 'git stash save'
    abbr gsa 'git stash apply'
    abbr gsd 'git stash drop'
    abbr gsp 'git stash pop'

    abbr gsu 'git submodule update'

    abbr gts 'git tag -s'

    abbr d 'git --git-dir=$HOME/build/dotfiles --work-tree=$HOME'
end

